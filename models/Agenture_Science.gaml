/**
* Name: AgentureScience
* Author: cburon
* Description: Mod�le pour le quatri�me TP du cours de l'ENSTA Syst�mes MultiAgents
*/

model Agenture_Science

global{
	string mortelle<-"mortelle";
	string normale<-"normale";
	string sortie<-"sortie";
	list<string> types_tuiles <- [mortelle, normale,sortie];
	int nb_cells <-10;
	int proba_mortelle<-10;
	float proba_changement<-1.0/(nb_cells*nb_cells);
	int nb_robot_detruits;
	int nb_robot_sauves;
	int nb_robots <-3;
	case case_depart;
	case case_sortie;
	init{
		case_depart <-any(case);
		case_sortie <-any(case where(not (each=case_depart)));
		case_depart.type_case<-normale;
		case_sortie.type_case<-sortie;
		nb_robot_detruits<-0;
		nb_robot_sauves<-0;
		create robotTest number:nb_robots with:[location::case_depart.location,speed::world.shape.width/nb_cells,ma_case::case_depart];
	}
}

grid case width:nb_cells height:nb_cells{
	string type_case;
	init{
		if(flip(proba_mortelle/100.0)){
			type_case<-mortelle;
		}
		else{
			type_case<-normale;
		}
	}
	reflex change_type when:(flip(proba_changement) and type_case!=sortie and empty(robotTest inside self)){
		if(flip(proba_mortelle/100.0)){
			type_case<-mortelle;
		}
		else{
			type_case<-normale;
		}
		ask list(robotTest){
				do remove_belief(new_predicate(mortelle,["case"::myself]));
				do remove_belief(new_predicate(sortie,["case"::myself]));
				do remove_belief(new_predicate(normale,["case"::myself]));
		}
	}
	reflex tue when:type_case=mortelle{
		ask robotTest inside self{
			do die;
			nb_robot_detruits <- nb_robot_detruits + 1;
		}
	}
	reflex sauve when:type_case=sortie{
		ask robotTest inside self{
			do die;
			nb_robot_sauves <- nb_robot_sauves + 1;
		}
	}
	aspect default{
		draw square(world.shape.width/nb_cells) border:#black color:type_case=sortie?#green:(type_case=mortelle?#red:#white);
	}
}

species robotTest skills:[moving,messaging] control:simple_bdi{
	path mon_chemin;
	case ma_case;
	case cible;
	int nb_explorees;
	robotTest a_avertir;
	predicate avertir <- new_predicate("avertir");
	predicate sortir <- new_predicate("sortir");
	predicate explorer <- new_predicate("explorer");
	bool probabilistic_choice<-true;
	float intention_persistence <- 1.0;
	float plan_persistence <- 1.0;
	
	
	list<case> n_degre(case c, int n){
		list<case> result <-[c];
		loop i from:1 to:n{
			list<case> a_ajouter <- [];
			loop c over: result{
				a_ajouter <<+ c.neighbors where (not(self in result));
			}
			result<-result union a_ajouter;
		}
		return result;
	}
	
	init{
		do add_desire(explorer) strength:8;
		do add_belief(new_predicate(normale,["case"::ma_case]));
		nb_explorees<-0;
	}
	perceive target:robotTest inside ma_case{
		focus communication_possible var:name;
	}

	plan determiner_case_a_voir intention:explorer when:mon_chemin=nil priority:10 finished_when: mon_chemin!=nil{
		write "deter";
		bool stop <- false;
		int i <-1;
		loop while: not stop{
			list<case> cases_croyance <-(get_beliefs(new_predicate(normale)) collect (case(get_predicate(mental_state (each)).values["case"]))) union (get_beliefs(new_predicate(mortelle)) collect (case(get_predicate(mental_state (each)).values["case"]))) union (get_beliefs(new_predicate(sortie)) collect (case(get_predicate(mental_state (each)).values["case"])));
			list<case> cases_aVoir <- n_degre(ma_case,i) - cases_croyance;
			list<case> cases_sures<-get_beliefs(new_predicate(normale)) collect (case(get_predicate(mental_state (each)).values["case"]));
			loop c over: cases_aVoir{
				if(!stop){
					write(c);
					mon_chemin <- path_between(case where (each in (cases_sures union list(c))), ma_case, c);
					if(mon_chemin!=nil){
						stop<-true;
						cible<-c;
					}
				}
			}
			i<-i+1;
			if(i=nb_cells){
				stop<-true;
			}
		}
	}
	
	plan coup_doeil intention:explorer priority:1 when:mon_chemin!=nil finished_when:(not empty(get_beliefs(new_predicate(sortie,["case"::cible])) union get_beliefs(new_predicate(normale,["case"::cible])) union get_beliefs(new_predicate(mortelle,["case"::cible])))) {
		write("cp oeil");
		point prec<-location;
		do follow path:mon_chemin;
		if(self.location=mon_chemin.target){//si on est arrivé à destination, on ne se déplace pas, on regarde. Ce "truc" est un peu moche mais on fait avec.
			location<-prec;
			case c <- any(case where(each=(mon_chemin.target as case)));
			do add_belief(new_predicate(c.type_case,["case"::c]));
			if(c.type_case=sortie){
				write(get_beliefs(new_predicate(sortie,["case"::c])));
				do remove_intention(explorer,false);
			}
			nb_explorees<-nb_explorees+1;
			mon_chemin<-nil;
		}
		ma_case<-location as case;
	}
	plan aller_sortie intention:sortir{
		write "sortir";
		list<case> cases_sures<-(get_beliefs(new_predicate(normale)) collect (case(get_predicate(mental_state (each)).values["case"])))union (get_beliefs(new_predicate(sortie)) collect (case(get_predicate(mental_state (each)).values["case"])));
		case sortie_trouvee <- any(get_beliefs(new_predicate(sortie)) collect (case(get_predicate(mental_state (each)).values["case"])));
		if(path_between(case where (each in cases_sures), ma_case, sortie_trouvee)=nil){
			do remove_intention(sortir, false);
			do add_intention(explorer);
		}
		else{
			do follow path: path_between(case where (each in cases_sures), ma_case, sortie_trouvee);
			ma_case<-location as case;
		}
	}
	plan determiner_agent_info intention:avertir when:a_avertir=nil priority:10{
		string nom_robot <- any(get_beliefs(avertir) collect (string(get_predicate(mental_state (each)).values["name_value"])));
		a_avertir<-any(robotTest where (name=nom_robot));
		do remove_belief(new_predicate("communication_possible"));
	}
	
	plan communiquer_info intention:avertir when:a_avertir!=nil finished_when:self.location=a_avertir.location priority:1{
		write "com info";
		do goto target:a_avertir;
		if(self.location=a_avertir.location){
			if(not empty(get_beliefs(new_predicate(sortie)))){
				ask a_avertir{
					do add_belief(new_predicate(sortie,["case"::any(get_beliefs(new_predicate(sortie))collect (case(get_predicate(mental_state (each)).values["case"])))]));
				}
			}
			else if(not empty(get_beliefs(new_predicate(mortelle)))){
				ask a_avertir{
					do add_belief(new_predicate(mortelle,["case"::any(get_beliefs(new_predicate(mortelle))collect (case(get_predicate(mental_state (each)).values["case"])))]));
				}
			}
			else if(not empty(get_beliefs(new_predicate(normale)))){
				ask a_avertir{
					do add_belief(new_predicate(normale,["case"::any(get_beliefs(new_predicate(normale))collect (case(get_predicate(mental_state (each)).values["case"])))]));
				}
			}
			do remove_desire(avertir);
			nb_explorees<-0;
		}
		ma_case<-location as case;
	}
	rule belief:new_predicate("communication_possible") new_desire:avertir strength:nb_explorees;
	rule belief:new_predicate(sortie) new_desire:sortir strength:100.0;
	
//	plan receive_chgt when:(not empty(mailbox)){FIXME Cépacomsakonfé
//		write "reception";
//		list<message> a_traiter<-mailbox;
//		loop m over:a_traiter{
//			if(string(m.contents) contains mortelle){
//				case c <- any(case where(name = (string(m.contents) split_with ";")[0]));
//				do add_belief(new_predicate(mortelle,["case"::c]));
//			}			
//			if(string(m.contents) contains sortie){
//				case c <- any(case where(name = (string(m.contents) split_with ";")[0]));
//				do add_belief(new_predicate(sortie,["case"::c]));
//			}			
//			if(string(m.contents) contains normale){
//				case c <- any(case where(name = (string(m.contents) split_with ";")[0]));
//				do add_belief(new_predicate(normale,["case"::c]));
//			}			
//			if(string(m.contents) contains "inconnu"){
//				case c <- any(case where(name = (string(m.contents) split_with ";")[0]));
//				do remove_belief(new_predicate(mortelle,["case"::c]));
//				do remove_belief(new_predicate(sortie,["case"::c]));
//				do remove_belief(new_predicate(normale,["case"::c]));
//			}
//		}
//	}
	
	aspect default{
		draw circle(2) color:#blue;
	}
}

/* Insert your model definition here */

experiment agenture_test{
	output{
		display salleTest{
			species case aspect:default;
//			grid case lines: #black;
			species robotTest aspect:default;
		}
	}
}